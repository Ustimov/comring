﻿namespace ComRing
{
    partial class ChatControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chatSplitContainer = new System.Windows.Forms.SplitContainer();
            this.chatMetroTextBox = new System.Windows.Forms.RichTextBox();
            this.inputSplitContainer = new System.Windows.Forms.SplitContainer();
            this.inputMetroTextBox = new MetroFramework.Controls.MetroTextBox();
            this.sendButton = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.chatSplitContainer)).BeginInit();
            this.chatSplitContainer.Panel1.SuspendLayout();
            this.chatSplitContainer.Panel2.SuspendLayout();
            this.chatSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputSplitContainer)).BeginInit();
            this.inputSplitContainer.Panel1.SuspendLayout();
            this.inputSplitContainer.Panel2.SuspendLayout();
            this.inputSplitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // chatSplitContainer
            // 
            this.chatSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chatSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.chatSplitContainer.Name = "chatSplitContainer";
            this.chatSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // chatSplitContainer.Panel1
            // 
            this.chatSplitContainer.Panel1.Controls.Add(this.chatMetroTextBox);
            // 
            // chatSplitContainer.Panel2
            // 
            this.chatSplitContainer.Panel2.Controls.Add(this.inputSplitContainer);
            this.chatSplitContainer.Size = new System.Drawing.Size(557, 351);
            this.chatSplitContainer.SplitterDistance = 300;
            this.chatSplitContainer.TabIndex = 0;
            // 
            // chatMetroTextBox
            // 
            this.chatMetroTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chatMetroTextBox.Location = new System.Drawing.Point(0, 0);
            this.chatMetroTextBox.Name = "chatMetroTextBox";
            this.chatMetroTextBox.ReadOnly = true;
            this.chatMetroTextBox.Size = new System.Drawing.Size(557, 300);
            this.chatMetroTextBox.TabIndex = 0;
            this.chatMetroTextBox.Text = "";
            // 
            // inputSplitContainer
            // 
            this.inputSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.inputSplitContainer.Name = "inputSplitContainer";
            // 
            // inputSplitContainer.Panel1
            // 
            this.inputSplitContainer.Panel1.Controls.Add(this.inputMetroTextBox);
            // 
            // inputSplitContainer.Panel2
            // 
            this.inputSplitContainer.Panel2.Controls.Add(this.sendButton);
            this.inputSplitContainer.Size = new System.Drawing.Size(557, 47);
            this.inputSplitContainer.SplitterDistance = 388;
            this.inputSplitContainer.TabIndex = 0;
            // 
            // inputMetroTextBox
            // 
            this.inputMetroTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputMetroTextBox.Location = new System.Drawing.Point(0, 0);
            this.inputMetroTextBox.MaxLength = 255;
            this.inputMetroTextBox.Multiline = true;
            this.inputMetroTextBox.Name = "inputMetroTextBox";
            this.inputMetroTextBox.Size = new System.Drawing.Size(388, 47);
            this.inputMetroTextBox.TabIndex = 0;
            // 
            // sendButton
            // 
            this.sendButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sendButton.Location = new System.Drawing.Point(0, 0);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(165, 47);
            this.sendButton.TabIndex = 0;
            this.sendButton.Text = "Отправить";
            // 
            // ChatControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chatSplitContainer);
            this.Name = "ChatControl";
            this.Size = new System.Drawing.Size(557, 351);
            this.chatSplitContainer.Panel1.ResumeLayout(false);
            this.chatSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chatSplitContainer)).EndInit();
            this.chatSplitContainer.ResumeLayout(false);
            this.inputSplitContainer.Panel1.ResumeLayout(false);
            this.inputSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.inputSplitContainer)).EndInit();
            this.inputSplitContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer chatSplitContainer;
        private System.Windows.Forms.SplitContainer inputSplitContainer;
        private MetroFramework.Controls.MetroTextBox inputMetroTextBox;
        private MetroFramework.Controls.MetroButton sendButton;
        private System.Windows.Forms.RichTextBox chatMetroTextBox;

    }
}
