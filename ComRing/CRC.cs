﻿/**
 * Created by Vadim on 19.04.2015. (https://github.com/Larionov-Vadim/)
 */

using System;
using System.Text;

namespace ComRing
{
    public class CRC
    {
        public static byte[] Encode(byte[] data)
        {
            byte[] result = new byte[data.Length * 2];
            byte currentByte, halfByte1, halfByte2, residue;

            for (int i = 0; i < data.Length; ++i)
            {
                currentByte = data[i];

                halfByte1 = (byte)((currentByte & 0xF) << 3);
                residue = Divide(halfByte1);
                result[2 * i] = (byte)(halfByte1 | residue);

                halfByte2 = (byte)((currentByte & 0xF0) >> (4 - 3));
                residue = Divide(halfByte2);
                result[2 * i + 1] = (byte)(halfByte2 | residue);
            }

            return result;
        }

        public static byte[] Decode(byte[] data)
        {
            byte[] result = new byte[data.Length / 2];
            byte currentByte, halfByte1, halfByte2, residue;

            for (int i = 0; i < data.Length - 1; i += 2)
            {
                currentByte = 0;
                halfByte1 = data[i];
                halfByte2 = data[i + 1];

                residue = Divide(halfByte1);
                if (residue != 0)
                {
                    throw new Exception("residue " + residue + "!= 0");
                }

                currentByte = (byte)(currentByte | (halfByte1 & 0x78) >> 3);

                residue = Divide(halfByte2);
                if (residue != 0)
                {
                    throw new Exception("residue " + residue + "!= 0");
                }

                currentByte = (byte)(currentByte | ((halfByte2 & 0x78) << 1));

                result[i / 2] = currentByte;
            }

            return result;
        }

        private static byte GetShiftNumbers(byte number, byte maxShift)
        {
            byte count = 0;
            for (byte b = 0x40; b != 0; b = (byte)(b >> 1))
            {
                if ((number & b) > 0)
                {
                    return count > maxShift ? maxShift : count;
                }
                ++count;
            }
            return 0;
        }

        private static byte Divide(byte dividend)
        {
            const byte divider = 0x58;
            byte maxShift = 3;
            byte residue = dividend;

            while ((residue) > 0x7)
            {
                residue = (byte)(residue ^ (divider >> GetShiftNumbers(residue, maxShift)));
            }
            return residue;
        }
    }
}
