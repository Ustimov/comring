﻿using System.Collections.Generic;

namespace ComRing
{
    class InitializationFrame
    {
        public List<int> addresses = new List<int>();
        public List<string> names = new List<string>();
        public FrameType frameType = FrameType.INIT;

        public InitializationFrame(int sourceAddress, string sourceName)
        {
            addresses.Add(sourceAddress);
            names.Add(sourceName);
        }

        public void AddDestination(int dstAddress, string dstName)
        {
            addresses.Add(dstAddress);
            names.Add(dstName);
        }

        public int Count
        {
            get { return names.Count; }
        }
    }
}
