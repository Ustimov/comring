﻿namespace ComRing
{
    enum FrameType
    {
        INIT = 1,
        MSG = 2,
        ACK = 3,
        LINK = 4,
    }
}
