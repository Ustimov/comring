﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using System.Timers;
using System.Threading;
using Newtonsoft.Json;

namespace ComRing
{
    public delegate void ConnectionReady(object source, ConnectionReadyEventArgs e);
    public delegate void MessageReceived(object source, MessageReceivedEventArgs e);

    public class ConnectionReadyEventArgs : EventArgs
    {
        public List<int> addresses = new List<int>();
        public List<string> names = new List<string>();
    }

    public class MessageReceivedEventArgs : EventArgs
    {
        public string nickName;
        public int sourceAddress;
        public int dstAddress;
        public string msg;
    }

    public class Manager
    {
        public event ConnectionReady OnConnectionReady;
        public event MessageReceived OnMessageReceived;

        private SerialPort serialPort;
        private SerialPort serialPort2;

        private System.Timers.Timer timer;

        private string nickName;
        private int address;

        private bool connectionReady = false;
        private bool isLinked = true;

        private string lastMsg = null;

        public SerialPort SerialPort
        {
            get { return serialPort; }
        }

        public SerialPort SerialPort2
        {
            get { return serialPort2; }
        }

        public Manager(string nickName, string portName, string portName2, int baudRate)
        {
            this.nickName = nickName;
            this.address = 1;

            serialPort = new SerialPort(portName, baudRate);
            serialPort2 = new SerialPort(portName2, baudRate);

            serialPort.Encoding = Encoding.UTF8;
            serialPort2.Encoding = Encoding.UTF8;

            serialPort.Handshake = Handshake.RequestToSendXOnXOff;
            serialPort2.Handshake = Handshake.RequestToSendXOnXOff;

            serialPort.Open();
            serialPort2.Open();

            serialPort2.DataReceived += serialPort2_DataReceived;
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //serialPort.Write(JsonConvert.SerializeObject(
            //    new TypeCheckFrame() { frameType = FrameType.LINK }));

            //if (isLinked)
            //{
            //    isLinked = false;
            //}
            if (!serialPort2.CDHolding)
            //else
            {
                if (OnMessageReceived != null)
                {
                    var eventArgs = new MessageReceivedEventArgs()
                    {
                        nickName = "Программа",
                        sourceAddress = 0xFF,
                        dstAddress = 0x7F,
                        msg = "Соединение потеряно. Возможно отключился пользователь, "
                            + "либо возникли неполадки на линии. Для повторного соединения "
                            + "необходимо устранить неполадки и перезапустить программу."
                    };
                    OnMessageReceived(this, eventArgs);
                }
                timer.Stop();
                serialPort.Close();
                serialPort2.Close();
            }

            
            //if (lastMsg != null)
            //{
            //    serialPort.Write(lastMsg);
            //}
            
        }

        void serialPort2_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Thread.Sleep(100);

            var str = serialPort2.ReadExisting();

            TypeCheckFrame typeCheckFrame;

            try
            {
                typeCheckFrame = JsonConvert.DeserializeObject<TypeCheckFrame>(str);
            }
            catch (JsonException)
            {
                serialPort2.DiscardInBuffer();

                if (!connectionReady)
                {
                    Connect();
                }
                return;
            } 

            if (typeCheckFrame.frameType == FrameType.INIT)
            {
                var initFrame = JsonConvert.DeserializeObject<InitializationFrame>(str);
                if (initFrame.Count < 3)
                {
                    address = initFrame.Count + 1;
                    initFrame.AddDestination(address, nickName);
                    serialPort.Write(JsonConvert.SerializeObject(initFrame));
                }
                else if (initFrame.Count == 3)
                {
                    if (OnConnectionReady != null)
                    {
                        if (connectionReady)
                        {
                            return;
                        }

                        var eventArgs = new ConnectionReadyEventArgs();
                        
                        foreach (var address in initFrame.addresses)
                        {
                            eventArgs.addresses.Add(address);
                        }
                        
                        foreach (var name in initFrame.names)
                        {
                            eventArgs.names.Add(name);
                        }
                        
                        OnConnectionReady(this, eventArgs);

                        connectionReady = true;

                        serialPort.Write(JsonConvert.SerializeObject(initFrame));
                        
                        timer = new System.Timers.Timer()
                        {
                            Interval = 1000,
                        };
                        timer.Elapsed += timer_Elapsed;
                        timer.Start();
                    }
                }
            }
            else if (typeCheckFrame.frameType == FrameType.MSG)
            {
                var msgFrame = JsonConvert.DeserializeObject<MessageFrame>(str);

                if (msgFrame.sourceAddress == address)
                {
                    return;
                }

                if (msgFrame.dstAddress == address || msgFrame.dstAddress == 0x7F)
                {
                    if (OnMessageReceived != null)
                    {
                        msgFrame.Decode();
                        var eventArgs = new MessageReceivedEventArgs()
                        {
                            nickName = msgFrame.nickName,
                            sourceAddress = msgFrame.sourceAddress,
                            dstAddress = msgFrame.dstAddress,
                            msg = msgFrame.msg
                        };
                        OnMessageReceived(this, eventArgs);

                        if (msgFrame.dstAddress == 0x7F)
                        {
                            serialPort.Write(str);
                        }
                        /*
                        else
                        {
                            serialPort.Write(JsonConvert.SerializeObject(
                                new AcknowledgeFrame(address, msgFrame.sourceAddress)));
                        }
                        */
                    }
                }
                else
                {
                    serialPort.Write(str);
                }
            }
            else if (typeCheckFrame.frameType == FrameType.LINK)
            {
                isLinked = true;
            }
            else if (typeCheckFrame.frameType == FrameType.ACK)
            {
                var ackFrame = JsonConvert.DeserializeObject<AcknowledgeFrame>(str);

                if (ackFrame.sourceAddress == address)
                {
                    return;
                }
                else if (lastMsg == null)
                {
                    serialPort.Write(str);
                    return;
                }

                var lastMsgFrame = JsonConvert.DeserializeObject<MessageFrame>(lastMsg);
                
                if (ackFrame.dstAddress == address &&
                    ackFrame.sourceAddress == lastMsgFrame.dstAddress)
                {
                    lastMsg = null;
                }
                else
                {
                    serialPort.Write(str);
                }
            }
        }

        public void Connect()
        {
            var initFrame = new InitializationFrame(address, nickName);
            serialPort.Write(JsonConvert.SerializeObject(initFrame));
        }

        public void SendMessage(string nickName, int sourceAddress, int dstAddress, string msg)
        {
            var msgFrame = new MessageFrame(nickName, sourceAddress, dstAddress, msg);
            msgFrame.Encode();
            lastMsg = JsonConvert.SerializeObject(msgFrame);
            serialPort.Write(lastMsg);

            if (msgFrame.dstAddress == 0x7F)
            {
                lastMsg = null;
            }
        }
    }
}
