﻿namespace ComRing
{
    class AcknowledgeFrame
    {
        public int sourceAddress;
        public int dstAddress;

        public FrameType frameType = FrameType.ACK;

        public AcknowledgeFrame(int sourceAddress, int dstAddress)
        {
            this.sourceAddress = sourceAddress;
            this.dstAddress = dstAddress;
        }
    }
}
