﻿namespace ComRing
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.nickNameMetroTextBox = new MetroFramework.Components.MetroStyleManager(this.components);
            this.chatMetroTabControl = new MetroFramework.Controls.MetroTabControl();
            this.sharedMetroTabPage = new MetroFramework.Controls.MetroTabPage();
            this.secondUserMetroTabPage = new MetroFramework.Controls.MetroTabPage();
            this.firstUserMetroTabPage = new MetroFramework.Controls.MetroTabPage();
            this.connectControl = new ComRing.ConnectControl();
            this.sharedChatControl = new ComRing.ChatControl();
            this.secondUserChatControl = new ComRing.ChatControl();
            this.firstUserChatControl = new ComRing.ChatControl();
            ((System.ComponentModel.ISupportInitialize)(this.nickNameMetroTextBox)).BeginInit();
            this.chatMetroTabControl.SuspendLayout();
            this.sharedMetroTabPage.SuspendLayout();
            this.secondUserMetroTabPage.SuspendLayout();
            this.firstUserMetroTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // nickNameMetroTextBox
            // 
            this.nickNameMetroTextBox.Owner = null;
            // 
            // chatMetroTabControl
            // 
            this.chatMetroTabControl.Controls.Add(this.sharedMetroTabPage);
            this.chatMetroTabControl.Controls.Add(this.secondUserMetroTabPage);
            this.chatMetroTabControl.Controls.Add(this.firstUserMetroTabPage);
            this.chatMetroTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chatMetroTabControl.Location = new System.Drawing.Point(20, 60);
            this.chatMetroTabControl.Name = "chatMetroTabControl";
            this.chatMetroTabControl.SelectedIndex = 0;
            this.chatMetroTabControl.Size = new System.Drawing.Size(307, 314);
            this.chatMetroTabControl.TabIndex = 14;
            this.chatMetroTabControl.Visible = false;
            // 
            // sharedMetroTabPage
            // 
            this.sharedMetroTabPage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sharedMetroTabPage.Controls.Add(this.sharedChatControl);
            this.sharedMetroTabPage.HorizontalScrollbarBarColor = true;
            this.sharedMetroTabPage.HorizontalScrollbarSize = 0;
            this.sharedMetroTabPage.Location = new System.Drawing.Point(4, 35);
            this.sharedMetroTabPage.Name = "sharedMetroTabPage";
            this.sharedMetroTabPage.Size = new System.Drawing.Size(299, 275);
            this.sharedMetroTabPage.TabIndex = 0;
            this.sharedMetroTabPage.Text = "Общий диалог";
            this.sharedMetroTabPage.VerticalScrollbarBarColor = true;
            this.sharedMetroTabPage.VerticalScrollbarSize = 0;
            // 
            // secondUserMetroTabPage
            // 
            this.secondUserMetroTabPage.Controls.Add(this.secondUserChatControl);
            this.secondUserMetroTabPage.HorizontalScrollbarBarColor = true;
            this.secondUserMetroTabPage.HorizontalScrollbarSize = 0;
            this.secondUserMetroTabPage.Location = new System.Drawing.Point(4, 35);
            this.secondUserMetroTabPage.Name = "secondUserMetroTabPage";
            this.secondUserMetroTabPage.Size = new System.Drawing.Size(299, 275);
            this.secondUserMetroTabPage.TabIndex = 2;
            this.secondUserMetroTabPage.Text = "Пользователь 2";
            this.secondUserMetroTabPage.VerticalScrollbarBarColor = true;
            this.secondUserMetroTabPage.VerticalScrollbarSize = 0;
            // 
            // firstUserMetroTabPage
            // 
            this.firstUserMetroTabPage.Controls.Add(this.firstUserChatControl);
            this.firstUserMetroTabPage.HorizontalScrollbarBarColor = true;
            this.firstUserMetroTabPage.HorizontalScrollbarSize = 0;
            this.firstUserMetroTabPage.Location = new System.Drawing.Point(4, 35);
            this.firstUserMetroTabPage.Name = "firstUserMetroTabPage";
            this.firstUserMetroTabPage.Size = new System.Drawing.Size(299, 275);
            this.firstUserMetroTabPage.TabIndex = 1;
            this.firstUserMetroTabPage.Text = "Пользователь 1";
            this.firstUserMetroTabPage.VerticalScrollbarBarColor = true;
            this.firstUserMetroTabPage.VerticalScrollbarSize = 0;
            // 
            // connectControl
            // 
            this.connectControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.connectControl.Location = new System.Drawing.Point(20, 60);
            this.connectControl.Name = "connectControl";
            this.connectControl.Size = new System.Drawing.Size(307, 314);
            this.connectControl.TabIndex = 15;
            // 
            // sharedChatControl
            // 
            this.sharedChatControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sharedChatControl.Location = new System.Drawing.Point(0, 0);
            this.sharedChatControl.Name = "sharedChatControl";
            this.sharedChatControl.Size = new System.Drawing.Size(297, 273);
            this.sharedChatControl.TabIndex = 2;
            // 
            // secondUserChatControl
            // 
            this.secondUserChatControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.secondUserChatControl.Location = new System.Drawing.Point(0, 0);
            this.secondUserChatControl.Name = "secondUserChatControl";
            this.secondUserChatControl.Size = new System.Drawing.Size(299, 275);
            this.secondUserChatControl.TabIndex = 2;
            // 
            // firstUserChatControl
            // 
            this.firstUserChatControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.firstUserChatControl.Location = new System.Drawing.Point(0, 0);
            this.firstUserChatControl.Name = "firstUserChatControl";
            this.firstUserChatControl.Size = new System.Drawing.Size(299, 275);
            this.firstUserChatControl.TabIndex = 2;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 394);
            this.Controls.Add(this.connectControl);
            this.Controls.Add(this.chatMetroTabControl);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Resizable = false;
            this.Text = "Соединение";
            ((System.ComponentModel.ISupportInitialize)(this.nickNameMetroTextBox)).EndInit();
            this.chatMetroTabControl.ResumeLayout(false);
            this.sharedMetroTabPage.ResumeLayout(false);
            this.secondUserMetroTabPage.ResumeLayout(false);
            this.firstUserMetroTabPage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Components.MetroStyleManager nickNameMetroTextBox;
        private MetroFramework.Controls.MetroTabControl chatMetroTabControl;
        private MetroFramework.Controls.MetroTabPage sharedMetroTabPage;
        private ChatControl sharedChatControl;
        private MetroFramework.Controls.MetroTabPage secondUserMetroTabPage;
        private ChatControl secondUserChatControl;
        private MetroFramework.Controls.MetroTabPage firstUserMetroTabPage;
        private ChatControl firstUserChatControl;
        private ConnectControl connectControl;

    }
}