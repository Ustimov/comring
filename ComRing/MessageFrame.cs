﻿namespace ComRing
{
    class MessageFrame
    {
        public string nickName;
        public int sourceAddress;
        public int dstAddress;
        public string msg;

        public FrameType frameType = FrameType.MSG;

        public MessageFrame(string nickName, int sourceAddress, int dstAddress, string msg)
        {
            this.sourceAddress = sourceAddress;
            this.nickName = nickName;
            this.dstAddress = dstAddress;
            this.msg = msg;
        }

        public void Encode()
        {
            msg = System.Text.Encoding.UTF8.GetString(
                CRC.Encode(System.Text.Encoding.UTF8.GetBytes(msg)));
        }

        public void Decode()
        {
            msg = System.Text.Encoding.UTF8.GetString(
                CRC.Decode(System.Text.Encoding.UTF8.GetBytes(msg)));
        }
    }
}
