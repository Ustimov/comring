﻿using System.Windows.Forms;
using MetroFramework.Controls;

namespace ComRing
{
    public partial class ChatControl : UserControl
    {
        public ChatControl()
        {
            InitializeComponent();
        }

        public MetroButton SendButton
        {
            get { return sendButton; }
        }

        public RichTextBox ChatMetroTextBox
        {
            get { return chatMetroTextBox; }
        }

        public MetroTextBox InputMetroTextBox
        {
            get { return inputMetroTextBox; }
        }
    }
}
