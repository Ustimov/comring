﻿namespace ComRing
{
    partial class ConnectControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.connectMetroButton = new MetroFramework.Controls.MetroButton();
            this.errorMetroTile = new MetroFramework.Controls.MetroTile();
            this.portNameMetroComboBox = new MetroFramework.Controls.MetroComboBox();
            this.portName2MetroComboBox = new MetroFramework.Controls.MetroComboBox();
            this.portNameMetroLabel = new MetroFramework.Controls.MetroLabel();
            this.portName2MetroLabel = new MetroFramework.Controls.MetroLabel();
            this.nickNameMetroTextBox = new MetroFramework.Controls.MetroTextBox();
            this.openMetroButton = new MetroFramework.Controls.MetroButton();
            this.nickNameMetroLabel = new MetroFramework.Controls.MetroLabel();
            this.baudRateMetroLabel = new MetroFramework.Controls.MetroLabel();
            this.baudRateMetroComboBox = new MetroFramework.Controls.MetroComboBox();
            this.SuspendLayout();
            // 
            // connectMetroButton
            // 
            this.connectMetroButton.Location = new System.Drawing.Point(27, 259);
            this.connectMetroButton.Name = "connectMetroButton";
            this.connectMetroButton.Size = new System.Drawing.Size(246, 23);
            this.connectMetroButton.TabIndex = 12;
            this.connectMetroButton.Text = "Установить соединение";
            // 
            // errorMetroTile
            // 
            this.errorMetroTile.Location = new System.Drawing.Point(25, 20);
            this.errorMetroTile.Name = "errorMetroTile";
            this.errorMetroTile.Size = new System.Drawing.Size(248, 23);
            this.errorMetroTile.TabIndex = 11;
            this.errorMetroTile.Text = "Поле \"Имя\" не должно быть пустым";
            this.errorMetroTile.Visible = false;
            // 
            // portNameMetroComboBox
            // 
            this.portNameMetroComboBox.FormattingEnabled = true;
            this.portNameMetroComboBox.ItemHeight = 23;
            this.portNameMetroComboBox.Location = new System.Drawing.Point(123, 92);
            this.portNameMetroComboBox.Name = "portNameMetroComboBox";
            this.portNameMetroComboBox.Size = new System.Drawing.Size(150, 29);
            this.portNameMetroComboBox.TabIndex = 2;
            // 
            // portName2MetroComboBox
            // 
            this.portName2MetroComboBox.FormattingEnabled = true;
            this.portName2MetroComboBox.ItemHeight = 23;
            this.portName2MetroComboBox.Location = new System.Drawing.Point(123, 135);
            this.portName2MetroComboBox.Name = "portName2MetroComboBox";
            this.portName2MetroComboBox.Size = new System.Drawing.Size(150, 29);
            this.portName2MetroComboBox.TabIndex = 10;
            // 
            // portNameMetroLabel
            // 
            this.portNameMetroLabel.AutoSize = true;
            this.portNameMetroLabel.Location = new System.Drawing.Point(27, 102);
            this.portNameMetroLabel.Name = "portNameMetroLabel";
            this.portNameMetroLabel.Size = new System.Drawing.Size(39, 19);
            this.portNameMetroLabel.TabIndex = 3;
            this.portNameMetroLabel.Text = "Порт";
            // 
            // portName2MetroLabel
            // 
            this.portName2MetroLabel.AutoSize = true;
            this.portName2MetroLabel.Location = new System.Drawing.Point(27, 144);
            this.portName2MetroLabel.Name = "portName2MetroLabel";
            this.portName2MetroLabel.Size = new System.Drawing.Size(50, 19);
            this.portName2MetroLabel.TabIndex = 9;
            this.portName2MetroLabel.Text = "Порт 2";
            // 
            // nickNameMetroTextBox
            // 
            this.nickNameMetroTextBox.Location = new System.Drawing.Point(123, 61);
            this.nickNameMetroTextBox.Name = "nickNameMetroTextBox";
            this.nickNameMetroTextBox.Size = new System.Drawing.Size(150, 23);
            this.nickNameMetroTextBox.TabIndex = 4;
            // 
            // openMetroButton
            // 
            this.openMetroButton.Location = new System.Drawing.Point(27, 225);
            this.openMetroButton.Name = "openMetroButton";
            this.openMetroButton.Size = new System.Drawing.Size(246, 23);
            this.openMetroButton.TabIndex = 8;
            this.openMetroButton.Text = "Открыть порты";
            // 
            // nickNameMetroLabel
            // 
            this.nickNameMetroLabel.AutoSize = true;
            this.nickNameMetroLabel.Location = new System.Drawing.Point(27, 64);
            this.nickNameMetroLabel.Name = "nickNameMetroLabel";
            this.nickNameMetroLabel.Size = new System.Drawing.Size(35, 19);
            this.nickNameMetroLabel.TabIndex = 5;
            this.nickNameMetroLabel.Text = "Имя";
            // 
            // baudRateMetroLabel
            // 
            this.baudRateMetroLabel.AutoSize = true;
            this.baudRateMetroLabel.Location = new System.Drawing.Point(27, 184);
            this.baudRateMetroLabel.Name = "baudRateMetroLabel";
            this.baudRateMetroLabel.Size = new System.Drawing.Size(65, 19);
            this.baudRateMetroLabel.TabIndex = 7;
            this.baudRateMetroLabel.Text = "Скорость";
            // 
            // baudRateMetroComboBox
            // 
            this.baudRateMetroComboBox.FormattingEnabled = true;
            this.baudRateMetroComboBox.ItemHeight = 23;
            this.baudRateMetroComboBox.Location = new System.Drawing.Point(123, 175);
            this.baudRateMetroComboBox.Name = "baudRateMetroComboBox";
            this.baudRateMetroComboBox.Size = new System.Drawing.Size(150, 29);
            this.baudRateMetroComboBox.TabIndex = 6;
            // 
            // ConnectControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.portNameMetroComboBox);
            this.Controls.Add(this.connectMetroButton);
            this.Controls.Add(this.baudRateMetroComboBox);
            this.Controls.Add(this.errorMetroTile);
            this.Controls.Add(this.baudRateMetroLabel);
            this.Controls.Add(this.nickNameMetroLabel);
            this.Controls.Add(this.portName2MetroComboBox);
            this.Controls.Add(this.openMetroButton);
            this.Controls.Add(this.portNameMetroLabel);
            this.Controls.Add(this.nickNameMetroTextBox);
            this.Controls.Add(this.portName2MetroLabel);
            this.Name = "ConnectControl";
            this.Size = new System.Drawing.Size(299, 308);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton connectMetroButton;
        private MetroFramework.Controls.MetroTile errorMetroTile;
        private MetroFramework.Controls.MetroComboBox portNameMetroComboBox;
        private MetroFramework.Controls.MetroComboBox portName2MetroComboBox;
        private MetroFramework.Controls.MetroLabel portNameMetroLabel;
        private MetroFramework.Controls.MetroLabel portName2MetroLabel;
        private MetroFramework.Controls.MetroTextBox nickNameMetroTextBox;
        private MetroFramework.Controls.MetroButton openMetroButton;
        private MetroFramework.Controls.MetroLabel nickNameMetroLabel;
        private MetroFramework.Controls.MetroLabel baudRateMetroLabel;
        private MetroFramework.Controls.MetroComboBox baudRateMetroComboBox;

    }
}
