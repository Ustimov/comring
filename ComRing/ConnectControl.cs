﻿using System.Windows.Forms;
using MetroFramework.Controls;

namespace ComRing
{
    public partial class ConnectControl : UserControl
    {
        public ConnectControl()
        {
            InitializeComponent();
        }

        public MetroButton ConnectMetroButton
        {
            get { return connectMetroButton; }
        }

        public MetroButton OpenMetroButton
        {
            get { return openMetroButton; }
        }

        public MetroTile ErrorMetroTile
        {
            get { return errorMetroTile; }
        }

        public MetroComboBox FirstPortNameMetroComboBox
        {
            get { return portNameMetroComboBox; }
        }

        public MetroComboBox SecondPortNameMetroComboBox
        {
            get { return portName2MetroComboBox; }
        }

        public MetroComboBox BaudRateMetroComboBox
        {
            get { return baudRateMetroComboBox; }
        }

        public MetroTextBox NickNameMetroTextBox
        {
            get { return nickNameMetroTextBox; }
        }
    }
}
