﻿using System;
using System.IO.Ports;
using MetroFramework.Forms;

namespace ComRing
{
    public partial class MainForm : MetroForm
    {
        private Manager manager;
        
        private ConnectionReadyEventArgs conReadyEventArgs;
        
        private string nickName;
        private int address;
        
        public MainForm()
        {
            InitializeComponent();

            connectControl.OpenMetroButton.Click += openMetroButton_Click;
            connectControl.ConnectMetroButton.Click += connectMetroButton_Click;

            firstUserChatControl.SendButton.Click += firstUserSendButton_Click;
            secondUserChatControl.SendButton.Click += secondUserSendButton_Click;
            sharedChatControl.SendButton.Click += sharedSendButton_Click;

            connectControl.FirstPortNameMetroComboBox
                .DataSource = SerialPort.GetPortNames();
            connectControl.SecondPortNameMetroComboBox
                .DataSource = SerialPort.GetPortNames();

            var baudRates = new string[] { "115200", "57600", "56000",
                "38400", "19200", "14400", "9600" };

            connectControl.BaudRateMetroComboBox.DataSource = baudRates;
        }

        void firstUserSendButton_Click(object sender, EventArgs e)
        {
            var str = firstUserChatControl.InputMetroTextBox.Text;
            
            if (str == String.Empty)
            {
                return;
            }

            firstUserChatControl.InputMetroTextBox.Text = String.Empty;

            firstUserChatControl.ChatMetroTextBox.AppendText(
                String.Format("{0}> {1}\n", nickName, str));

            manager.SendMessage(nickName, address,
                Convert.ToInt32(firstUserMetroTabPage.Tag), str);
        }

        void secondUserSendButton_Click(object sender, EventArgs e)
        {
            var str = secondUserChatControl.InputMetroTextBox.Text;

            if (str == String.Empty)
            {
                return;
            }

            secondUserChatControl.InputMetroTextBox.Text = String.Empty;

            secondUserChatControl.ChatMetroTextBox.AppendText(
                String.Format("{0}> {1}\n", nickName, str));

            manager.SendMessage(nickName, address,
                Convert.ToInt32(secondUserMetroTabPage.Tag), str);
        }

        void sharedSendButton_Click(object sender, EventArgs e)
        {
            var str = sharedChatControl.InputMetroTextBox.Text;

            if (str == String.Empty)
            {
                return;
            }

            sharedChatControl.InputMetroTextBox.Text = String.Empty;

            sharedChatControl.ChatMetroTextBox.AppendText(
                String.Format("{0}> {1}\n", nickName, str));

            manager.SendMessage(nickName, address, 0x7F, str);
        }

        private void openMetroButton_Click(object sender, EventArgs e)
        {
            if (connectControl.NickNameMetroTextBox.Text == String.Empty)
            {
                connectControl.ErrorMetroTile.Visible = true;
                return;
            }

            this.nickName = connectControl.NickNameMetroTextBox.Text;

            manager = new Manager(
                connectControl.NickNameMetroTextBox.Text,
                connectControl.FirstPortNameMetroComboBox.Text,
                connectControl.SecondPortNameMetroComboBox.Text,
                Convert.ToInt32(connectControl.BaudRateMetroComboBox.Text));
            
            manager.OnConnectionReady += manager_OnConnectionReady;
            manager.OnMessageReceived += manager_OnMessageReceived;

            manager.SerialPort.PinChanged += SerialPort_PinChanged;
            manager.SerialPort2.PinChanged += SerialPort_PinChanged;

            connectControl.ErrorMetroTile.Visible = false;

            connectControl.OpenMetroButton.Enabled = false;
            connectControl.NickNameMetroTextBox.Enabled = false;
            connectControl.FirstPortNameMetroComboBox.Enabled = false;
            connectControl.SecondPortNameMetroComboBox.Enabled = false;
            connectControl.BaudRateMetroComboBox.Enabled = false;
        }

        void SerialPort_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            var p = e.EventType;
        }

        void manager_OnMessageReceived(object source, MessageReceivedEventArgs e)
        {
            if (e.dstAddress == 0x7F)
            {
                Invoke(new Action(() =>
                {
                    if (e.sourceAddress == 0xFF)
                    {
                        chatMetroTabControl.Enabled = false;
                    }

                    sharedChatControl.ChatMetroTextBox.AppendText(
                        String.Format("{0}> {1}\n", e.nickName, e.msg));
                    chatMetroTabControl.SelectedTab = sharedMetroTabPage;
                }));
            }
            else
            {
                var firstUserAddress = Convert.ToInt32(firstUserMetroTabPage.Tag);
                var secondUserAddress = Convert.ToInt32(secondUserMetroTabPage.Tag);
                
                if (e.sourceAddress == firstUserAddress)
                {
                    Invoke(new Action(() =>
                    {
                        firstUserChatControl.ChatMetroTextBox.AppendText(
                            String.Format("{0}> {1}\n", e.nickName, e.msg));
                        chatMetroTabControl.SelectedTab = firstUserMetroTabPage;
                    }));
                }
                else if (e.sourceAddress == secondUserAddress)
                {
                    Invoke(new Action(() =>
                    {
                        secondUserChatControl.ChatMetroTextBox.AppendText(
                            String.Format("{0}> {1}\n", e.nickName, e.msg));
                        chatMetroTabControl.SelectedTab = secondUserMetroTabPage;
                    }));
                }
            }
        }

        void manager_OnConnectionReady(object source, ConnectionReadyEventArgs e)
        {
            conReadyEventArgs = e;

            Invoke(new Action(() =>
            {
                var index = conReadyEventArgs.names.FindIndex(
                    name => { return name == nickName; });
                address = conReadyEventArgs.addresses[index];

                Text = String.Format("Диалоги ({0})", nickName);

                conReadyEventArgs.names.RemoveAt(index);
                conReadyEventArgs.addresses.RemoveAt(index);

                firstUserMetroTabPage.Text = conReadyEventArgs.names[0];
                firstUserMetroTabPage.Tag = conReadyEventArgs.addresses[0];

                secondUserMetroTabPage.Text = conReadyEventArgs.names[1];
                secondUserMetroTabPage.Tag = conReadyEventArgs.addresses[1];

                connectControl.Visible = false;
                chatMetroTabControl.Visible = true;

                Resizable = true;

                Refresh();
            }));
        }

        private void connectMetroButton_Click(object sender, EventArgs e)
        {
            manager.Connect();
            connectControl.ConnectMetroButton.Enabled = false;
        }
    }
}
